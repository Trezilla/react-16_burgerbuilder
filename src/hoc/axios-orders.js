import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://react16-burger-6698e.firebaseio.com/'
});

export default instance;